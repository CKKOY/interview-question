import AppContainer from "./container";
import React from "react";
import ReactDOM from "react-dom";

const wrapper = document.getElementById("container");
wrapper ? ReactDOM.render(<AppContainer />, wrapper) : false;