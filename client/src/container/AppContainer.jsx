import React, { Component } from "react";
import ReactDOM from "react-dom";

export default class AppContainer extends Component {
 render() {
  return (
    <div>
      <div>
        <h1>My video list</h1>
      </div>
      <div>
        <span>Send a GET request to /api/videos to get the video list</span>
      </div>
    </div>);
 }
}