const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
  output: {
    filename: 'bundle.js',
    path: `${__dirname}/../server/src/main/webapp/resources/scripts`
  },
  module: {
    rules: [{
      test: /\.js(x)?$/,
      exclude: /node_modules/,
      use: {
        loader: "babel-loader"
      }
    }, {
      test: /\.html$/,
      use: [{
        loader: "html-loader"
      }]
    }]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "../../index.html"
    })
  ],
  resolve: {
    extensions: ['.js', '.json', '.jsx']
  }
};