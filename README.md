#Smartzer Interview Question

##Task
Using this project, please create a system for: viewing all my videos; viewing an individual video's information (this does not have to show the actual video file); adding a new video; removing a selected video; and the ability to update a video's information. When we refer to a video here, we mean the video's meta data and not the actual video file. A video has the following meta data associated with it: name, duration, source and id. The list of current videos are stored in a Map and for this task, that is an acceptable data storage method. Please feel free to add any extra features as well

##Submitting
Please fork this project and send us your completed git link

##Server
The server side of the project can be found in the server folder. It is a Java project using RESTeasy as the REST framework and it is built using gradle. You can start the server using `gradle appRun`

##Client
The client side of the project can be found in the client folder. It is a React project and is built using webpack. You can use `npm run build` to build the project. It will update the index.jsp file within the server automatically and hot deploy if the server is running


