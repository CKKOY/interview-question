package com.smartzer.controllers;


import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.smartzer.beans.Video;

@Produces("application/json")
@Path("videos")
public class VideosController {
	static Map<Long, Video> products = new HashMap<Long, Video>();
	
	static {
		products.put(1L, new Video(1, "Test video", 6300, ""));
		products.put(2L, new Video(2, "Christmas video", 75000, ""));
	}
	
	@GET
	public Response getVideos() {
		return Response.status(200).entity(products).build();
	}
}
